# Untitled Minecraft Server Project

[[_TOC_]]

## Rules of Play

- Respect the property of others, don't be intentionally disruptive towards them or make large changes to their buildings, structures, or environments without consent. We expect a person's property to be intuitive to people, and disputes should be settled between involved players if possible.

- We want the server to incorporate community spaces, when you build, please try to be considerate of your surroundings. We don't want dirt huts and pillars cluttering or surrounding other peoples things.

- Player killing is not allowed without consent. We want people to have fun so if you make a cool pvp arena or minigame or are just screwing around with friends then that's great, but we don't want people griefing each other in the open world.

- Abide by the rules of another player on their property; e.g. if a player owns a store, purchase items according to their instructions. If a player runs a game, follow the rules of the game.

- If a person creates a unique RP situation, try to engage with it sincerely and follow the rules set by the player within their experience.

- This server will be using an in-game voice chat as well as text chat. Please be mindful of others while using these features.

- Have fun with friends! The goal of the server is to try to create a fun and friendly community, we want to avoid a collection of people playing singleplayer together. (This is not a rule, just heavily encouraged.)

## Getting Whitelisted

- If you want to get whitelisted, post in the #vouches channel on discord with a message that includes your Minecraft username and @ pings the person who vouches for you. Once you are approved you will be added to the whitelist and given a whitelist role in the discord.

- If you are vouching for others, make sure to react to their message in the #vouches channel with a ⛏️ emoji to give them access to the server.

## Updating your Launcher

### Manual Modding Guide

- Download and install Fabric from here: https://fabricmc.net/use/

- Open your "%appdata%" folder and go into ".minecraft", then open (or create) a folder called "mods". 

- Download the "Fabric API" from here: https://www.curseforge.com/minecraft/mc-mods/fabric-api
and place it into your mods folder. 

- Download "Sodium" from here: https://www.curseforge.com/minecraft/mc-mods/sodium
and place it into your mods folder. 

- Download "Simple Voice Chat" from here: https://www.curseforge.com/minecraft/mc-mods/simple-voice-chat
and place it into your mods folder. 

- **Alternatively** you can use the "curseforge" mod loader, as explained below

### Modding with Curseforge

- Download and Install Curseforge here https://download.curseforge.com/

- Choose "Minecraft" and then in the top right corner click on Create Custom Profile

- Type in any name for the profile, preferably one you will recognize, then under Modloader click "Fabric" leave the version as the default

- Open up the profile now by clicking on it (by default you'll have Fabric API Installed)

- Click on Add More Content above the mod list to the right. From here you can search for each mod, the required ones being "Sodium" and "Simple Voice Chat"

- And now you're good to go!
